module Main exposing (..)

import Html
import Svg exposing (..)
import Svg.Attributes exposing (x, y, cx, cy, r, width, height, fill, stroke, color)


type BST
    = Empty
    | BST Int BST BST



----------Dvojiško iskalno drevo-------------------------
-- add : Int -> BST -> BST
-- depth : BST -> Int
-- makeTree : List Int -> BST
-- dfs : BST -> List Int
-- bfs : BST -> List Int
----------Risanje drevesa-------------------------
-- draw : List ( Int, Int, Int ) -> List (Svg.Svg msg)
--getCoords : Int -> Int -> BST -> List ( Int, Int, Int )


main =
    Html.pre [] [ Html.text "Zdravo Elm@FRI" ]
