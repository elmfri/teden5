# Naloge v petem tednu Elm@FRI



## Naloga 1
Osnovno delo z dvojiškimi iskalnimi drevesi.

```elm
type BST
    = Empty
    | BST Int BST BST
```
Implementirajte funkcije

```elm
add : Int -> BST -> BST
depth : BST -> Int
makeTree : List Int -> BST
dfs : BST -> List Int
bfs : BST -> List Int
```

1. Prva funkcija sprejme število in dvojiško drevo, vrne novo drevo z dodanim številom
2. Druga funkcija vrne globino (višino) podanega drevesa.
3. Funkcija 'makeTree' iz podanega seznama števil zgradi dvojiško iskalno drevo.
4. Funkcija 'dfs' vrne seznam števil iz drevesa, v vrstnem redu "iskanja v globino".
5. Funkcija 'bfs' vrne seznam števil iz drevesa, v vrstnem redu "iskanja v širino".


## Naloga 2.

Obstaja zelo veliko strategij za izris binarnega drevesa. Strategija, ki jo morate implementirati v tej funkciji, pa je sledeča: predstavljate si, da imate polno drevo (do največje globine so prisotna čisto vsa vozlišča).
Vsakemu vozlišču lahko dodelimo koordinati x in y, po sledečem pravilu:
 ```
x = zaporedna številka vozlišča i v inorder obhodu polnega drevesa
y = globina na kateri se nahaja vozlišče i v polnem drevesu.
 ```

 Najprej definirajte funkcijo :

 ```elm
 getCoords : BST -> List (Int,Int,Int)
 ```
 ki sprejme dvojiško drevo, in vrne seznam trojk (x, y, el) - torej seznam koordinat (x, y), na katerih se celo število el nahaja. V tem seznamu naj se nahajajo zgolj vozlišča, ki so dejansko prisotna v drevesu.

 Potem pa implementirajte še funkcijo

 ```elm
 draw : List ( Int, Int, Int ) -> List (Svg.Svg msg)
 ```
ki sprejme naračunani seznam koordinat z ustreznimi elementi in ga izriše v Svg-ju (ne potrebujete risati povezav med vozlišči drevesa).

En primer zelo enostavne vizualizacije, ki prikazuje drevo:

```elm
BST 5 (BST 3 (BST 2 Empty Empty) (BST 4 Empty Empty)) (BST 6 (BST 5 Empty Empty) (BST 7 (BST 6 Empty Empty) Empty))
```

![alt text](primer_drevesa.png)
